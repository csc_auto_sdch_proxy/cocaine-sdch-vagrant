package main

import (
	"github.com/cocaine/cocaine-framework-go/cocaine"
	"net/http"
)

func handler(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "text/plain")
	w.Write([]byte("This is an example server.\n"))
}

func main() {
	binds := map[string]cocaine.EventHandler{
		"example": cocaine.WrapHandlerFunc(handler, nil),
	}
	if worker, err := cocaine.NewWorker(); err == nil {
		worker.Loop(binds)
	} else {
		panic(err)
	}
}
