# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANTFILE_API_VERSION = '2'

Vagrant.require_version '>= 1.5.4'

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.omnibus.chef_version = '11.6.0'

  #  Plain precise64 box with kernel upgraded to 3.8 and GRUB_CMDLINE_LINUX="cgroup_enable=memory swapaccount=1"
  #  config.vm.box = 'precise64-docker'
  #  config.vm.box_url = 'https://github.com/cocaine/cocaine-vagrant/releases/download/v0.11/precise64-docker.box'

  # box with preinstalled common packages for faster up. Using only in development. Production should start on clean box
  config.vm.box = 'precise64-docker-preconfigured-v2'
  config.vm.box_url = 'https://www.dropbox.com/s/0kwlyjbrsqgt6n6/precise64-docker-preconfigured-v2.box?dl=1'


  config.berkshelf.enabled = true
  config.vm.provider 'virtualbox' do |v|
    v.memory = 1024
  end

  config.vm.provision :chef_solo do |chef|
    chef.add_recipe 'apt::default'
  end

  # common
  config.vm.provision 'user_env', type: :chef_solo do |chef|
    chef.add_recipe 'optional_user_enviroment'
  end


  config.vm.define 'elliptics' do |elliptics|
    elliptics.vm.network 'public_network', ip: '192.168.1.55', bridge: 'en0: Wi-Fi (AirPort)'
    elliptics.vm.provision :chef_solo do |chef|
      chef.add_recipe 'elliptics::install'
    end

    elliptics.vm.provision :chef_solo, run: 'always' do |chef|
      chef.add_recipe 'elliptics::configure_and_run'
    end

  end


  config.vm.define 'cocaine', primary: true do |cocaine|
    cocaine.vm.network :forwarded_port, guest: 80, host: 48080
    cocaine.vm.network 'public_network', ip: '192.168.1.50', bridge: 'en0: Wi-Fi (AirPort)'

    cocaine.vm.provision 'cocaine', type: :chef_solo do |chef|
      chef.log_level = :debug
      chef.add_recipe 'curl'
      chef.add_recipe 'python'
      chef.add_recipe 'cocaine::install'
    end


    # GOLANG provision part
    cocaine.vm.provision 'golang', type: :chef_solo do |chef|
      chef.json = {
          'go' => {
              'version' => '1.6',
              #          'owner' => 'vagrant'
          }
      }
      chef.add_recipe 'golang'
    end

    if ENV['DEVELOPMENT'] == 'YES' then #link dev repos from host $GOPATH to cocaine $GOPATH
      puts 'Warning: Run on Developer machine'
      go_dev_src_synced_folder = '/'
      cocaine.vm.synced_folder "#{ENV['GOPATH']}/src",
                               "#{go_dev_src_synced_folder}/src",
                               create: true
      cocaine.vm.provision 'link_golang_src', type: :shell,
                           inline: "ln -snf #{go_dev_src_synced_folder}/src $GOPATH/src"
    end
    # GOLANG provision part end


    cocaine.vm.provision 'cocaine_restart', type: :chef_solo, run: 'always' do |chef|
      chef.add_recipe 'cocaine::configure_and_run'
      chef.add_recipe 'cocaine::go_example'
      chef.add_recipe 'cocaine::flask_example'
      chef.add_recipe 'cocaine::qr_example'
    end

    cocaine.vm.provision 'sdch_restart', type: :chef_solo, run: 'always' do |chef|
      #chef.add_recipe 'sdch::chooser'
      chef.add_recipe 'sdch::taker'
    end
  end

  config.vm.define 'mongo' do |mongo|
    mongo.vm.network 'public_network', ip: '192.168.1.70', bridge: 'en0: Wi-Fi (AirPort)'
    mongo.vm.provision :chef_solo do |chef|
      chef.add_recipe 'mongodb3::default'
    end

  end

  config.vm.define 'elliptics_client', autostart: false do |elliptics_client|
    elliptics_client.vm.network 'public_network', ip: '192.168.1.60', bridge: 'en0: Wi-Fi (AirPort)'
    elliptics_client.vm.provision :chef_solo do |chef|
      chef.add_recipe 'elliptics::client'
    end
  end

end

