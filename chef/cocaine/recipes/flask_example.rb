package 'python-flask' do
  options '--force-yes'
end

bash 'Installing flask example' do
  cwd '/vagrant/examples/flask'
  code 'cocaine-tool app upload --name flask && cocaine-tool app restart --name flask --profile default'
end

bash 'testing flask example' do
  cwd '/vagrant/examples/flask'
  code './flask_test_install.py'
end
