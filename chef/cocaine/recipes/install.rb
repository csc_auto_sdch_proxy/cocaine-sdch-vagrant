include_recipe 'python'

bash 'Turn on reverbrain repo' do
  code <<-EOH
cp -v /vagrant/chef/cocaine/repo.reverbrain.conf /etc/apt/sources.list.d/reverbrain.list

curl http://repo.reverbrain.com/REVERBRAIN.GPG | apt-key add -

apt-get update
  EOH
end

package 'libcocaine-core2' do
  options '--force-yes'
end

package 'cocaine-runtime' do
  options '--force-yes'
end

python_pip 'cocaine' do
  version '0.11.1.7'
  action :install
end

python_pip 'cocaine-tools' do
  version '0.11.6.1'
  action :install
end


package 'elliptics-client' do
  options '--force-yes'
  version '2.24.14.31'
end

package 'libcocaine-plugin-elliptics' do
  options '--force-yes'
  version '2.24.14.31'
end

package 'libjpeg-dev' do
    options '--force-yes'
end

python_pip 'pillow'


