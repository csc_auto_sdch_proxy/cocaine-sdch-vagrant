
cocaine_framework_package = "github.com/cocaine/cocaine-framework-go/cocaine"
#cocaine_framework_tag = '0.11-2'
#   git checkout tags/#{cocaine_framework_tag}
cocaine_framework_commit = '31df7e8b5d80ec52a03d896d505c6530fd30c5bf'

if ENV['GOPATH'] == '' then
  raise 'no $GOPATH specified'
end
cocaine_framework_location = "#{ENV['GOPATH']}/src/#{cocaine_framework_package}"
bash 'install go framework if it does not  exitst' do
  #user 'vagrant' #gopath owner
  code <<-EOH
    go get #{cocaine_framework_package} 
    cd #{cocaine_framework_location} 
    git checkout #{cocaine_framework_commit}
  EOH
  not_if { ::File.exists?(cocaine_framework_location) }
end
