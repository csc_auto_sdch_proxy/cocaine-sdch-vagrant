bash 'Configuring cocaine-runtime and proxy' do
  cwd '/vagrant/examples'
  code <<-EOH
  mkdir -p /etc/cocaine
  cp /vagrant/examples/cocaine-tornado-proxy.conf /etc/cocaine/cocaine-tornado-proxy.conf

  cat <<EOF > /etc/default/cocaine-runtime
CONFIG_PATH="/etc/cocaine/cocaine.conf"
RUNTIME_PATH="/var/run/cocaine"
EOF

  cp /vagrant/cocaine.conf /etc/cocaine/cocaine.conf
  EOH
end

runtime_run_output = "/tmp/output.txt"
file runtime_run_output do
    action :delete
end

cocaine_run_cmd = 'service cocaine-runtime restart'
bash cocaine_run_cmd do
    cwd '/vagrant/examples'
    code <<-EOH
    #{cocaine_run_cmd} &> #{runtime_run_output}
    EOH
end

ruby_block "Print cocaine-runtime run output" do
    only_if { ::File.exists?(runtime_run_output) }
    block do
        print "\n"
        cocaine_error = false
        File.open(runtime_run_output).each do |line|
            print line
            if ( line =~ /ERROR(.*)/ )
                cocaine_error = true
            end
        end
        if (cocaine_error) 
          File.open(runtime_run_output).each do |line|
              STDERR.puts line
              STDERR.puts "\n"
          end
          raise "\n Cocaine-runtime restart failed!!!"
        end
    end
end

bash 'Creating cocaine app control objects' do
  cwd '/vagrant/examples'
  retries 2
  code <<-EOH
  cocaine-tool runlist create --name default
  cocaine-tool profile upload --name default --profile='{"pool-limit": 4, "isolate": {"type": "process", "args": {"spool": "/var/spool/cocaine"}}}'
  EOH
end

bash 'Bootstrapping' do
  cwd '/'
  code 'cocaine-tool proxy start --daemon --port=80'
end
