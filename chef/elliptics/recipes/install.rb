include_recipe 'elliptics::client'

package 'eblob' do
  options '--force-yes -y'
  version '0.21.26'
end

package 'elliptics' do
  options '--force-yes'
  version '2.24.14.31'
end

directory '/tmp/history' do
  action :create
  owner 'vagrant'
end

directory '/tmp/root' do
  action :create
  owner 'vagrant'
end

server_conf_path = '/home/vagrant/ioserv.conf'

bash 'Copy conf' do 
  code <<-EOH
    cp /vagrant/chef/elliptics/ioserv.conf  #{server_conf_path}
    EOH
end

bash 'Run server' do 
  code <<-EOH
    dnet_ioserv -c #{server_conf_path}
    EOH
end






