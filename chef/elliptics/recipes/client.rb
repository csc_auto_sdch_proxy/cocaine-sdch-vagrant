bash 'Turn on reverbrain repo' do
  code <<-EOH
cp -v /vagrant/chef/elliptics/repo.reverbrain.conf /etc/apt/sources.list.d/reverbrain.list

curl http://repo.reverbrain.com/REVERBRAIN.GPG | apt-key add -

apt-get update
  EOH
end

package 'elliptics-client' do
  options '--force-yes'
  version '2.24.14.31'
end


