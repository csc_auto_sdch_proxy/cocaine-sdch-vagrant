default['sdch']['build_dir'] = '/tmp/sdch-build'
default['sdch']['repo'] = 'bitbucket.com/csc_auto_sdch_proxy'
default['sdch']['services'] = ['chooser', 'taker', 'getter', 'generator', 'chooser']

for service in default['sdch']['services'] 
  default['sdch'][service]['package'] = "#{default['sdch']['repo']}/#{service}/cmd/#{service}"
end
