app_name = 'chooser'

attr = node.default['sdch']
app_build_dir = "#{attr['build_dir']}/#{app_name}"

remote_directory app_build_dir do
    source app_name
    action :create

    # owner node[:user][:username]
    # group node[:user][:username]
    mode '0755'

    recursive true
    overwrite true
    purge     true
end


bash "get and build #{app_name}" do
  code <<-EOH
    set -e
    go get -t #{attr[app_name]['package']}
    cp -f $GOBIN/#{app_name} #{app_build_dir}
  EOH
end

bash "upload and restart #{app_name}" do
  cwd app_build_dir
  code <<-EOH
    set -e
    cocaine-tool app upload  --name #{app_name} && \
    cocaine-tool app restart --name #{app_name} --profile default
  EOH
end

